// Fill out your copyright notice in the Description page of Project Settings.


#include "Decrease.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ADecrease::ADecrease()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADecrease::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADecrease::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADecrease::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DeleteSnakeElement();

			int RandomX = FMath::RandRange(-450, 450);
			int RandomY = FMath::RandRange(-1080, 330);
			this->SetActorLocation(FVector(RandomX, RandomY, 0));
		}
	}
}

