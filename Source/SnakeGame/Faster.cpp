// Fill out your copyright notice in the Description page of Project Settings.


#include "Faster.h"
#include "SnakeBase.h"

// Sets default values
AFaster::AFaster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFaster::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFaster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFaster::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{

			int RandomX = FMath::RandRange(-450, 450);
			int RandomY = FMath::RandRange(-1080, 330);
			this->SetActorLocation(FVector(RandomX, RandomY, 0));

			Snake->SetActorTickInterval(Snake->MovementSpeed *= 0.8);
		}
	}
}

