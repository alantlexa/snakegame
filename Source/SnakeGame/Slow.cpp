// Fill out your copyright notice in the Description page of Project Settings.


#include "Slow.h"
#include "SnakeBase.h"

// Sets default values
ASlow::ASlow()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASlow::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASlow::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{

			int RandomX = FMath::RandRange(-450, 450);
			int RandomY = FMath::RandRange(-1080, 330);
			this->SetActorLocation(FVector(RandomX, RandomY, 0));

			Snake->SetActorTickInterval(Snake->MovementSpeed *= 1.2);
		}
	}
}

